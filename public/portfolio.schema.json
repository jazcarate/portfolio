{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "https://florius.com.ar/portfolio.shema.json",
    "type": "object",
    "title": "Portfolio",
    "properties": {
        "videos": {
            "type": "object",
            "properties": {
                "playlist": {
                    "$id": "#/items/videos/playlist",
                    "type": "string",
                    "title": "YouTube playlist id"
                },
                "items": {
                    "type": "array",
                    "title": "Videos",
                    "description": "All videos to be displayed.",
                    "default": [],
                    "examples": [
                        {
                            "name": "Foo",
                            "slug": "some-slug"
                        },
                        {
                            "name": "Bar",
                            "slug": "some-other-slug"
                        }
                    ],
                    "items": {
                        "$id": "#/items/video",
                        "type": "object",
                        "title": "Video",
                        "description": "A video to display.",
                        "required": [
                            "name",
                            "slug"
                        ],
                        "properties": {
                            "name": {
                                "$id": "#/items/video/name",
                                "type": "string",
                                "title": "Name",
                                "description": "Name of the video.",
                                "examples": [
                                    "foo"
                                ]
                            },
                            "slug": {
                                "$id": "#/items/video/slug",
                                "type": "string",
                                "title": "YouTube slug",
                                "description": "YouTube slug of the video.",
                                "$comment": "YouTube id",
                                "examples": [
                                    "some-slug"
                                ]
                            }
                        }
                    }
                }
            }
        },
        "projects": {
            "type": "array",
            "title": "Projects",
            "description": "All projects to be displayed.",
            "default": [],
            "examples": [
                [
                    {
                        "name": "Foo",
                        "url": "http://foo.example.com/",
                        "description": "A foo'sticle dependency.",
                        "challenges": [
                            "Writing an example schema."
                        ],
                        "source": "https://github.com/example/foo",
                        "tags": [
                            {
                                "name": "foo-lang",
                                "type": "language"
                            }
                        ]
                    },
                    {
                        "name": "bar",
                        "url": "https://bar.example.com/",
                        "description": "You wouldn't bar-live it.",
                        "challenges": [
                            "Writing two example schema!"
                        ]
                    }
                ]
            ],
            "items": {
                "$id": "#/items/project",
                "type": "object",
                "title": "Project",
                "description": "A project to display.",
                "required": [
                    "name",
                    "description",
                    "challenges",
                    "tags"
                ],
                "properties": {
                    "name": {
                        "$id": "#/items/project/name",
                        "type": "string",
                        "title": "Name",
                        "description": "Name of the project.",
                        "examples": [
                            "foo"
                        ]
                    },
                    "url": {
                        "$id": "#/items/project/url",
                        "type": "string",
                        "title": "URL",
                        "description": "URL of the project.",
                        "$comment": "Where can one see the thing live",
                        "examples": [
                            "http://foo.example.com/"
                        ]
                    },
                    "description": {
                        "$id": "#/items/project/description",
                        "type": "string",
                        "title": "Description",
                        "description": "Description of the project (HTML is allowed).",
                        "examples": [
                            "A foo'sticle dependency."
                        ]
                    },
                    "challenges": {
                        "$id": "#/items/project/challenges",
                        "type": "array",
                        "title": "Challenges",
                        "description": "Challenges of the project.",
                        "$comment": "How was this project challenging. What was interesting about it.",
                        "minItems": 1,
                        "examples": [
                            [
                                "Writing an example schema.",
                                "Writing two example schema!"
                            ]
                        ],
                        "items": {
                            "$id": "#/items/project/challenges/items",
                            "type": "string",
                            "title": "A single challenge"
                        }
                    },
                    "source": {
                        "$id": "#/items/project/source",
                        "type": "string",
                        "title": "Source Code URL",
                        "description": "URL for the source code.",
                        "examples": [
                            "https://github.com/example/foo"
                        ]
                    },
                    "tags": {
                        "$id": "#/items/project/tags",
                        "type": "array",
                        "title": "Tags",
                        "description": "Tags of the project.",
                        "examples": [
                            [
                                {
                                    "name": "Kotlin",
                                    "type": "language"
                                },
                                {
                                    "name": "Arrow",
                                    "type": "framework"
                                }
                            ]
                        ],
                        "items": {
                            "$id": "#/items/project/tags/items",
                            "type": "object",
                            "title": "Tag",
                            "description": "One tag of a project.",
                            "examples": [
                                {
                                    "name": "Kotlin",
                                    "type": "language"
                                }
                            ],
                            "required": [
                                "name",
                                "type"
                            ],
                            "properties": {
                                "name": {
                                    "$id": "#/items/project/tags/items/properties/name",
                                    "type": "string",
                                    "title": "Tag name",
                                    "description": "The name to be displayed.",
                                    "examples": [
                                        "Kotlin"
                                    ]
                                },
                                "type": {
                                    "$id": "#/items/project/tags/items/properties/type",
                                    "type": "string",
                                    "title": "Tag type",
                                    "description": "The type of the tag.",
                                    "enum": [
                                        "language",
                                        "technology",
                                        "framework"
                                    ],
                                    "examples": [
                                        "language"
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}